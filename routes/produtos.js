const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

const Produto = require("../mongo/models/produtos");

router.get("/", (req, res, next) => {
  Produto.find()
    .exec()
    .then(docs => {
      console.log(docs);
      //   if (docs.length >= 0) {
      res.status(200).json(docs);
      //   } else {
      //       res.status(404).json({
      //           message: 'No entries found'
      //       });
      //   }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post("/", (req, res, next) => {
  const produtos = new Produto({
    _id: new mongoose.Types.ObjectId(),
    nome: req.body.nome,
    preco: req.body.preco
  });
  produtos
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Handling POST requests to /produtos",
        createdProduto: result
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get("/:produtosId", (req, res, next) => {
  const id = req.params.produtosId;
    Produto.findById(id)
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("/:produtosId", (req, res, next) => {
  const id = req.params.produtosId;
  const updateOps = {};
  for (const ops of req.bwody) {
    updateOps[ops.propName] = ops.value;
  }
  Produto.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.delete("/:produtosId", (req, res, next) => {
  const id = req.params.produtosId;
  Produto.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;
