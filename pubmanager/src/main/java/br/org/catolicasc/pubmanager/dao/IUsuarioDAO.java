package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import br.org.catolicasc.pubmanager.entity.Usuario;

public interface IUsuarioDAO {
    public void createUsuario(Usuario usuario);
    
    public Usuario getUsuarioById(long id);
    
    public List<Usuario> getAllUsuarios();
    
    public void updateUsuario(Usuario usuario);
    
    public void deleteUsuario(long id);
}
