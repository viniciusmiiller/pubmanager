package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.org.catolicasc.pubmanager.entity.Produto;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ProdutoDAOImpl implements IProdutoDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void createProduto(Produto produto) {
		entityManager.persist(produto);
	}
	
	@Override
	public Produto getProdutoById(long id) {
		return entityManager.find(Produto.class, id);
	}

	@Override
	public List<Produto> getAllProdutos() {
		return entityManager.createQuery("select s from Produto s").getResultList();
	}

	@Override
	public void updateProduto(Produto produto) {
		entityManager.merge(produto);
	}

	@Override
	public void deleteProduto(long id) {
		Produto s = entityManager.find(Produto.class, id);
		entityManager.remove(s);
	}
	

}