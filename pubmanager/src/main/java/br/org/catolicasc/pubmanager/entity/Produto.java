package br.org.catolicasc.pubmanager.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Produto implements Bean{

	@Id
	@GeneratedValue
	private Long id;
	private String nome;
	private double preco;
	private String descricao;
	

	
	@JsonBackReference
	@OneToOne(mappedBy="produto", cascade = CascadeType.REFRESH)
	private Pedido pedido;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeServico() {
		return nome;
	}

	public void setNomeServico(String nomeServico) {
		this.nome = nomeServico;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	
}
