package br.org.catolicasc.pubmanager.dao;
import java.util.List;

import br.org.catolicasc.pubmanager.entity.Cliente;


public interface IClienteDAO
{
    public void createStudent(Cliente cliente);
    
    public Cliente getClienteById(long id);
    
    public List<Cliente> getClienteByNome(String nome);
    
    public List<Cliente> getAllClientes();
    
    public void updateCliente(Cliente cliente);
    
    public void deleteCliente(long id);
}