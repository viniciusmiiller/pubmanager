package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.org.catolicasc.pubmanager.entity.Pedido;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PedidoDAOImpl implements IPedidoDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void createPedido(Pedido pedido) {
		entityManager.persist(pedido);
	}

	@Override
	public Pedido getPedidoById(long id) {
		return entityManager.find(Pedido.class, id);
	}

	@Override
	public List<Pedido> getAllPedidos() {
		return entityManager.createQuery("select p from Pedido p").getResultList();
	}

	@Override
	public void updatePedido(Pedido pedido) {
		entityManager.merge(pedido);
	}

	@Override
	public void deletePedido(long id) {
		Pedido s = entityManager.find(Pedido.class, id);
		entityManager.remove(s);
	}

}