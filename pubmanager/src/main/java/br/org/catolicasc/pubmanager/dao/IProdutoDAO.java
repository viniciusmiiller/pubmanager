package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import br.org.catolicasc.pubmanager.entity.Produto;

public interface IProdutoDAO {
    public void createProduto(Produto produto);
    
    public Produto getProdutoById(long id);
    
    public List<Produto> getAllProdutos();
    
    public void updateProduto(Produto produtos);
    
    public void deleteProduto(long id);
}
