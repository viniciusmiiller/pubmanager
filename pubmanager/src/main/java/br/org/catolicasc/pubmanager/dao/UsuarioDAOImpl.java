package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.org.catolicasc.pubmanager.entity.Usuario;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UsuarioDAOImpl implements IUsuarioDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void createUsuario(Usuario usuario) {
		entityManager.persist(usuario);
	}

	@Override
	public Usuario getUsuarioById(long id) {
		return entityManager.find(Usuario.class, id);
	}

	@Override
	public List<Usuario> getAllUsuarios() {
		return entityManager.createQuery("select u from Usuario u").getResultList();
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		entityManager.merge(usuario);
	}

	@Override
	public void deleteUsuario(long id) {
		Usuario s = entityManager.find(Usuario.class, id);
		entityManager.remove(s);
	}

}
