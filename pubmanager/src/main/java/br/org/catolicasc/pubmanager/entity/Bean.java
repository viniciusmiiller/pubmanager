package br.org.catolicasc.pubmanager.entity;

import java.io.Serializable;

public interface Bean{

	Long getId();
	void setId(Long id);
}
