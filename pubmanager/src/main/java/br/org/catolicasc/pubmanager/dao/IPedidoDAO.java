package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import br.org.catolicasc.pubmanager.entity.Pedido;

public interface IPedidoDAO {
    public void createPedido(Pedido pedido);
    
    public Pedido getPedidoById(long id);
    
    public List<Pedido> getAllPedidos();
    
    public void updatePedido(Pedido pedidos);
    
    public void deletePedido(long id);
}
