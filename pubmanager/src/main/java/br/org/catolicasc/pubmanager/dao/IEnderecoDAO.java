package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import br.org.catolicasc.pubmanager.entity.Endereco;

public interface IEnderecoDAO {

    public void createEndereco(Endereco endereco);
    
    public Endereco getEnderecoById(long id);
    
    public List<Endereco> getAllEnderecos();
    
    public void updateEndereco(Endereco endereco);
    
    public void deleteEndereco(long id);
	
}
