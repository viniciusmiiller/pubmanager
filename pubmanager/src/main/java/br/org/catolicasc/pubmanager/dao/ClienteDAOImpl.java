package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.org.catolicasc.pubmanager.entity.Cliente;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class ClienteDAOImpl implements IClienteDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void createStudent(Cliente cliente) {
		entityManager.persist(cliente);
	}

	@Override
	public Cliente getClienteById(long id) {
		return entityManager.find(Cliente.class, id);
	}

	@Override
	public List<Cliente> getAllClientes() {
		return entityManager.createQuery("select stu from Cliente stu").getResultList();
	}

	@Override
	public void updateCliente(Cliente cliente) {
		entityManager.merge(cliente);
	}

	@Override
	public void deleteCliente(long id) {
		Cliente s = entityManager.find(Cliente.class, id);
		entityManager.remove(s);
	}

	@Override
	public List<Cliente> getClienteByNome(String nome) {

		List cliente = entityManager.createQuery("select  id, nome, cpf from Cliente WHERE nome=" + "'" + nome + "'")
				.getResultList();
		return cliente;
	}

}