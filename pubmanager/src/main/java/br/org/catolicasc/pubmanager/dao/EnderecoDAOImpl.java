package br.org.catolicasc.pubmanager.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.org.catolicasc.pubmanager.entity.Endereco;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class EnderecoDAOImpl implements IEnderecoDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void createEndereco(Endereco endereco) {
		entityManager.persist(endereco);
	}

	@Override
	public Endereco getEnderecoById(long id) {
		return entityManager.find(Endereco.class, id);
	}

	@Override
	public List<Endereco> getAllEnderecos() {
		return entityManager.createQuery("select e from Endereco e").getResultList();
	}

	@Override
	public void updateEndereco(Endereco endereco) {
		entityManager.merge(endereco);
	}

	@Override
	public void deleteEndereco(long id) {
		Endereco s = entityManager.find(Endereco.class, id);
		entityManager.remove(s);
	}

}