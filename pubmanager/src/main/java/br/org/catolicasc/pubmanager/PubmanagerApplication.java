package br.org.catolicasc.pubmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PubmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PubmanagerApplication.class, args);
	}
}
