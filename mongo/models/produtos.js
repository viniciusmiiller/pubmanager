const mongoose = require('mongoose');

const produtoSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	nome: String,
	imagem: String,
	preco: Number,
	quantidade: Number,
	date: { type: Date, default: Date.now },
	status: { type: Boolean, default: true }
}, { collection: 'produto' } 
);
//para exportar JSON
module.exports = mongoose.model('Produto', produtoSchema);


module.exports = function () {
	let { db } = require('./../../libs/connect-db')();
	let Schema = require('mongoose').Schema;

	return db.model('produto', Schema({
		nome: String,
		imagem: String,
		preco: Number,
		quantidade: Number,
		date: { type: Date, default: Date.now },
		status: { type: Boolean, default: true }
	}, { collection: 'produto' }));
}
